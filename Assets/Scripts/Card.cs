﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {

	public int cardID; /*11, 12, 13 BRONZE
						 21, 22, 23 SILVER
						 31, 32, 33 GOLD*/
	public int sellValue;
	GameObject eventSystem, child;
	public Sprite bronze1, bronze2, silver1, 
	silver2, gold1;

	void Awake(){
		eventSystem = GameObject.Find ("EventSystem");
	}

	void Start(){
		switch (cardID) {
		case 11:
			GetComponent<Image> ().sprite = bronze1;
			sellValue = 5;
			break;

		case 12:
			GetComponent<Image>().sprite = bronze2;
			sellValue = 5;
			break;

		case 21:
			GetComponent<Image>().sprite = silver1;
			sellValue = 10;
			break;

		case 22:
			GetComponent<Image>().sprite = silver2;
			sellValue = 10;
			break;

		case 31:
			GetComponent<Image>().sprite = gold1;
			sellValue = 15;
			break;
		}

		if (transform.parent.name == "BoughtPanel") {
			GetComponent<Draggable> ().enabled = false;
		}
	}

	void Update(){
		if (transform.parent.name == "SellPanel") {
			eventSystem.GetComponent<BuyPacks> ().currency += sellValue;
			Destroy (this.gameObject);
		}
	}
}