﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSaving : MonoBehaviour {

	public GameObject cardPanel;
	public List<GameObject> cardsInInv = new List<GameObject>();

	void Start(){
		SaveCards ();
		GetComponent<UIBehaviour> ().cardsToSpawn = cardsInInv.Count;
	}

	public void SaveCards () {
		cardsInInv.Clear ();
		int childrenInInv = cardPanel.transform.childCount;
		for(int i = 0; i < childrenInInv; i++){
			cardsInInv.Add (cardPanel.transform.GetChild(i).gameObject);
		}
	}
}