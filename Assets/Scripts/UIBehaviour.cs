﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBehaviour : MonoBehaviour {

	public GameObject quitButton, canvas, sellText, sellPanel, shopButton;
	public GameObject shopPanel, boughtPanel, invPanel;
	public int cardsToSpawn;
	public string cardIDsToGive;
	public GameObject cardGameobject;
	public List<int> cardIdsToLoad = new List<int>();
	public string cardIDs;
	//List of ID's split for each card
	public string[] cardIDsSplit;

	void Awake(){
		cardIDsToGive = PlayerPrefs.GetString ("CardIDs", "");
		cardIDsSplit = cardIDsToGive.Split ("/"[0]);
		cardsToSpawn = PlayerPrefs.GetInt ("InvCards", 0);
		GetComponent<BuyPacks> ().currency = PlayerPrefs.GetInt ("Currency", 200);

		for(int i = 0; i < cardsToSpawn; i++){
			Instantiate (cardGameobject, invPanel.transform);
			invPanel.transform.GetChild (i).GetComponent<Card> ().cardID = int.Parse(cardIDsSplit [i]);
		}
		cardsToSpawn = 0;
	}

	public void QuitGame(){
		quitButton.GetComponent<AudioSource> ().Play ();
		GetComponent<CardSaving> ().SaveCards ();
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#endif

		#if UNITY_STANDALONE
		Application.Quit();
		#endif

		cardIdsToLoad.Clear ();
		foreach(Transform t in invPanel.transform){
			if (t.GetComponentInChildren<Card> () != null) {
				Debug.Log ("added");
				cardIdsToLoad.Add (t.GetComponentInChildren<Card> ().cardID);
			}
		}
			
		foreach(int ID in cardIdsToLoad){
			cardIDs += ID.ToString () + "/";
		}
		PlayerPrefs.SetInt ("InvCards", GetComponent<CardSaving>().cardsInInv.Count);

		if(cardIDs == null)
		cardIDs = cardIDs.Remove (cardIDs.Length -1, 1);
		
		Debug.Log(cardIDs.ToString());
		PlayerPrefs.SetString ("CardIDs", cardIDs);
		PlayerPrefs.SetInt ("Currency", GetComponent<BuyPacks>().currency);
	}

	public void OpenShop(){
		canvas.GetComponent<AudioSource> ().Play ();
		shopPanel.SetActive (true);
		GetComponent<BuyPacks> ().buyB.interactable = true;
		GetComponent<BuyPacks> ().buyS.interactable = true;
		GetComponent<BuyPacks> ().buyG.interactable = true;
		sellText.SetActive (false);
		sellPanel.SetActive (false);
		shopButton.SetActive (false);
	}

	public void Back(){
		canvas.GetComponent<AudioSource> ().Play ();
		int boughtCards = boughtPanel.transform.childCount;
		for (int i = boughtCards - 1; i >= 0; i--) {
			boughtPanel.transform.GetChild (i).SetParent (invPanel.transform);
		}
		shopPanel.SetActive (false);
		sellText.SetActive (true);
		sellPanel.SetActive (true);
		shopButton.SetActive (true);
		GetComponent<CardSaving> ().SaveCards ();
	}
}