﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyPacks : MonoBehaviour {

	public GameObject spawnPanel;
	public int currency = 200;
	public GameObject card, canvas;
	public GameObject currencyText;
	public int diffPack;
	public Button buyB, buyS, buyG;
	public GameObject fireWorks, fireWorks2, fireworksound;

	void Start(){
		currencyText.GetComponent<Text>().text = currency.ToString () + " Coins";
	}
	void Update(){
		currencyText.GetComponent<Text>().text = currency.ToString () + " Coins";
	}

	public void buyBronzePack(){
		canvas.GetComponent<AudioSource> ().Play ();
		fireworksound.GetComponent<AudioSource>().Play();
		if(currency >= 10){
			if (GetComponent<UIBehaviour> ().invPanel.transform.childCount < 42) {
				diffPack = 0;
				Instantiate (card, spawnPanel.transform);
				Instantiate (card, spawnPanel.transform);
				Instantiate (card, spawnPanel.transform);
				currency -= 10;
				buyB.interactable = false;
				buyS.interactable = false;
				buyG.interactable = false;
				fireWorks.GetComponent<ParticleSystem> ().Play ();
				fireWorks2.GetComponent<ParticleSystem> ().Play ();
			}
		}
	}

	public void buySilverPack(){
		canvas.GetComponent<AudioSource> ().Play ();
		fireworksound.GetComponent<AudioSource>().Play();
		if(currency >= 20){
			if (GetComponent<UIBehaviour> ().invPanel.transform.childCount < 42) {
				diffPack = 1;
				Instantiate (card, spawnPanel.transform);
				Instantiate (card, spawnPanel.transform);
				Instantiate (card, spawnPanel.transform);
				currency -= 20;
				buyB.interactable = false;
				buyS.interactable = false;
				buyG.interactable = false;
				fireWorks.GetComponent<ParticleSystem> ().Play ();
				fireWorks2.GetComponent<ParticleSystem> ().Play ();
			}
		}
	}

	public void buyGoldPack(){
		canvas.GetComponent<AudioSource> ().Play ();
		fireworksound.GetComponent<AudioSource>().Play();
		if(currency >= 30){
			if (GetComponent<UIBehaviour> ().invPanel.transform.childCount < 42) {
				diffPack = 2;
				Instantiate (card, spawnPanel.transform);
				Instantiate (card, spawnPanel.transform);
				Instantiate (card, spawnPanel.transform);
				currency -= 30;
				buyB.interactable = false;
				buyS.interactable = false;
				buyG.interactable = false;
				fireWorks.GetComponent<ParticleSystem> ().Play ();
				fireWorks2.GetComponent<ParticleSystem> ().Play ();
			}
		}
	}
}