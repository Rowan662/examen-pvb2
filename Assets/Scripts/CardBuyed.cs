﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardBuyed : MonoBehaviour {

	public int cardID;
	public GameObject eventSystem, child;
	public Sprite bronze1, bronze2, silver1, 
	silver2, gold1;
	public int sellValue;

	void Awake(){
		eventSystem = GameObject.Find ("EventSystem");

		int[] chancesBronze = new int[]{11, 12, 11, 12, 21, 22};
		int[] chancesSilver = new int[]{11, 12, 21, 22, 21, 22, 31};
		int[] chancesGold = new int[]{11, 12, 21, 22, 21, 22, 21, 22, 31, 31};

		int randomIndexBronze = Random.Range (0,6);
		int randomIndexSilver = Random.Range (0,7);
		int randomIndexGold = Random.Range (0,10);

		int randomIntFromNumbersBronze = chancesBronze [randomIndexBronze];
		int randomIntFromNumbersSilver = chancesSilver [randomIndexSilver];
		int randomIntFromNumbersGold = chancesGold [randomIndexGold];

		switch (eventSystem.GetComponent<BuyPacks>().diffPack) {
		case 0:
			cardID = randomIntFromNumbersBronze;
			break;

		case 1:
			cardID = randomIntFromNumbersSilver;
			break;

		case 2:
			cardID = randomIntFromNumbersGold;
			break;
		}
	}

	void Start(){
		
		GetComponent<Draggable> ().enabled = false;

		switch (cardID) {
		case 11:
			GetComponent<Image> ().sprite = bronze1;
			sellValue = 5;
			break;

		case 12:
			GetComponent<Image>().sprite = bronze2;
			sellValue = 5;
			break;

		case 21:
			GetComponent<Image>().sprite = silver1;
			sellValue = 10;
			break;

		case 22:
			GetComponent<Image>().sprite = silver2;
			sellValue = 10;
			break;
		
		case 31:
			GetComponent<Image>().sprite = gold1;
			sellValue = 15;
			break;
		}
	}

	void Update(){
		if(transform.parent == eventSystem.GetComponent<UIBehaviour>().invPanel.transform){
			GetComponent<Draggable> ().enabled = true;
			GetComponent<Card> ().cardID = cardID;
		}
		if (transform.parent.name == "SellPanel") {
			eventSystem.GetComponent<BuyPacks> ().currency += sellValue;
			Destroy (this.gameObject);
		}
	}
}
